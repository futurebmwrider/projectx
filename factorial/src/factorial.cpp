#include <iostream>


//classwork 2, Ex 10.

using namespace std;

int main()
{
    int a;
    int r = 1;
    cout << "Enter the number: ";
    cin >> a;
    if (cin.fail())
    {
        cout << "Error";
    }
    else 
    {
        for (int i = 1; i <= a; i++)
        {
            r *= i;
        }
        cout << "Factorial equals: " << r;
    }
    return 0;
}

