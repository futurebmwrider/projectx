#include <iostream>

//classwork 2, Ex 4.

using namespace std;

int main()
{
    int a;
    cout << "Enter the maximum number:";
    cin >> a;
    int *b = new int[a + 1];
    for (int i = 0; i < a + 1; i++)
    {
        b[i] = i;
    }
    cout << "Prime numbers:" << endl;
    for (int q = 2; q < a + 1; q++)
    {
        if (b[q] != 0)
        {
            cout << b[q] << endl;
            for (int j = q * q; j < a + 1; j += q)
                b[j] = 0;
        }
    }
    return 0;
}
