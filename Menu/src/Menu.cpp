#include <iostream>

//classwork 2, Ex 8.

using namespace std;

int menu()
{
    int variant;
    cout << "Choose variant" << endl;
    cout << "1. Find the sum of numbers\n"
        << "2. Find the difference of numbers\n"
        << "3. Find the result of multiplying numbers\n"
        << "4. Find the result of division of numbers\n"
        << "5. Find the greatest common factor(GCF)\n"
        << "6. Find the factorial of a number\n"
        << "7. Exit\n" << endl;
    cout << ">>> ";
    cin >> variant;
    return variant;
}

int main()
{
    int variant = menu();

    int a, b, result;
    int r = 1;
        switch (variant) {
        case 1:
            cout << "Enter the numbers:";
            cin >> a >> b;
            result = a + b;
            cout << "The sum of numbers is:" << result << endl;
            break;
        case 2:
            cout << "Enter the numbers:";
            cin >> a >> b;
            result = a - b;
            cout << "The difference of numbers is:" << result << endl;
            break;
        case 3:
            cout << "Enter the numbers:";
            cin >> a >> b;
            result = a * b;
            cout << "The multiplication of numbers is:" << result << endl;
            break;
        case 4:
            cout << "Enter the numbers:";
            cin >> a >> b;
            result = a / b;
            cout << "The division of numbers is:" << result << endl;
            break;
        case 5:
            cout << "Enter two numbers:";
            cin >> a >> b;
            for (int i = a; i > 0; i--) {
                if (a % i == 0 && b % i == 0)
                {
                    cout << "GCF is:" << i << endl;
                    break;
                }
            }
                break;
        case 6:
            cout << "Enter the number:";
            cin >> a;
            for (int i = 1; i <= a; i++)
            {
                r *= i;
            }
            cout << "Factorial equals: " << r;
            break;
        case 7:
            cout << "Program completion:" << endl;
            exit(EXIT_SUCCESS);
            break;
        default:
            cerr << "Error" << endl;
            exit(EXIT_FAILURE);
                }
                return 0;
 }