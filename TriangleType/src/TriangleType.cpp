#include <iostream>

//classwork 2, Ex 6.

using namespace std;

int main()
{
    int a, b, c;
    cout << "Enter three sides of a triangle:";
    cin >> a >> b >> c;
    if (cin.fail() || 0 >= a == b == c)
    {
        cout << "Error.";
    }
    else if (a * a == b * b + c * c || b * b == a * a + c * c || c * c == b * b + a * a)
    {
        cout << "It is right triangle.";
    }
    else if (a >= b + c || b >= a + c || c >= b + a)
    {
        cout << "No such triangle";
    }
    else if (a == b && a != c || a == c && a != b || b == c && b != a)
    {
        cout << "It is isosceles triangle.";
    }
    else if (a == b && a == c)
    {
        cout << "It is equilateral triangle.";
    }
    else if (a != b && a != c && b != c)
    {
        cout << "It is miscellaneous triangle.";
    }
    return 0;
}
